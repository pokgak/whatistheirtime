/* eslint-disable no-undef */
import { zones } from "./assets/zones";

import { DateTime } from "luxon";
import { DateTime as DateTimePicker } from "vue-datetime";
import "vue-datetime/dist/vue-datetime.css";

var testInput = "2020-03-20T18:01:34+01:00";
var defaultTz = zones[0];

Vue.component("timebox", {
  props: ["time"],
  data: function() {
    return {
      zones: zones,
      tz: "local",
      format: DateTime.DATETIME_MED
    };
  },
  computed: {
    timeInTz: function() {
      return DateTime.fromISO(this.time)
        .setZone(this.tz)
        .toLocaleString(this.format);
    }
  },
  methods: {
    updateTimezone: function(event) {
      this.tz = event.target.value;
    }
  },
  template: `
    <div class="level is-mobile box">
      <div class="select is-small level-left">
        <select v-on:change="updateTimezone">
          <option v-for="z in zones" v-bind:value="z">{{ z }}</option>
        </select>
      </div>
      <p class="level-right">{{ timeInTz }}</p>
    </div>
  `
});

Vue.component("datetime", DateTimePicker);

// eslint-disable-next-line no-unused-vars
var mv = new Vue({
  el: "#app",
  data: {
    sourceDatetime: DateTime.fromISO(testInput)
      .setZone(defaultTz)
      .toISO(),
    timeboxCount: 0
  },
  methods: {
    addTimebox: function() {
      this.timeboxCount++;
    }
  }
});
