# What Is Their Time

What is their time (WITT) is a webapp where you can enter a time and see what is the equivalent time at chosen time zone(s). It is not limited to the current time only, you can specify any time of the day.

## Goals

Scalable, client-side logic. Preferably no maintenance of any servers.

## Technical Details

Information about how this project is written.

- runs client-side on the browser. Write in Javascript (and nodejs).
- frontend: VueJS because I need to learn it for an interview :P
- serverless deployment with Cloudflare Workers.

## How does it work

Users first choose a date and time they want to compare. Then, they'll need to choose the source timezones and timezones that they want to compare it to.
